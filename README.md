PROJET HMEE307
===================

Installez d'abord les dépendances en exécutant dans un terminal:

````
sudo apt-get update
sudo apt-get install build-essential python-dev python-smbus
````



Assurez-vous que la bibliothèque RPi.GPIO du Raspberry Pi est installée:

````
sudo pip install RPi.GPIO
````

Exécutez ce qui suit pour s'assurer que la bibliothèque Adafruit_BBIO est installée:

````
sudo pip install Adafruit_BBIO
````

Exécutez ensuite la commande suivante dans le répertoire de la bibliothèque en faisant un clone sur ce répertoire:

````
sudo python setup.py install
````

Pour lancer le code:

````
cd script
sudo python test.py
````
